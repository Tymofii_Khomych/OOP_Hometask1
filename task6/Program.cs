﻿namespace task6
{
    internal class Program
    {
        class Address
        {
            public string index { get; set; }
            public string country { get; set; }
            public string city { get; set; }
            public string street { get; set; }
            public string house { get; set; }
            public string apartment { get; set; }

            public Address()
            {
                index = string.Empty;
                country = string.Empty;
                city = string.Empty;
                street = string.Empty;
                house = string.Empty;
                apartment = string.Empty;
            }

            public Address(string index, string country, string city, string street, string house, string apartment)
            {
                this.index = index;
                this.country = country;
                this.city = city;
                this.street = street;
                this.house = house;
                this.apartment = apartment;
            }

            public void Show()
            {
                Console.WriteLine($"Address:\n\t{country}, {city}, {street}, {house}, {house}, {index}");
            }
        }
        static void Main(string[] args)
        {
            Address myAddress = new("12345", "USA", "New York", "Broadway", "1", "101");
            myAddress.Show();
        }
    }
}