﻿namespace task3
{
    internal class Program
    {
        class Book
        {
            private string title { get; set; }
            private string author { get; set; }
            private string content { get; set; }

            public Book()
            {
                title = string.Empty;
                author = string.Empty;
                content = string.Empty;
            }

            public Book(string title, string author, string content)
            {
                this.title = title;
                this.author = author;
                this.content = content;
            }

            public void Show(string title, string author, string content)
            {
                Console.ForegroundColor = ConsoleColor.Magenta;
                Console.WriteLine(title);
                Console.ForegroundColor = ConsoleColor.Blue;
                Console.WriteLine(author);
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine(content);
                Console.ForegroundColor = ConsoleColor.Gray;
            }
        }

        class Title
        {
            public string content = string.Empty;
            public string Content
            {
                internal get
                {
                    if (content != null)
                        return content;
                    else
                        return "Title is empty";
                }
                set
                {
                    content = value;
                }
            }

            public void Show()
            {
                Console.WriteLine($"Title: {this.content}");
            }
        }

        class Author
        {
            private string content = string.Empty;
            public string Content
            {
                internal get
                {
                    if (content != null)
                        return content;
                    else
                        return "Auhor is not mentioned";
                }
                set
                {
                    content = value;
                }
            }

            public void Show()
            {
                Console.WriteLine($"Author: {this.content}");
            }
        }

        class Content
        {
            private string text = string.Empty;
            public string Text
            {
                internal get
                {
                    if (text != null)
                        return text;
                    else
                        return "Content is empty";
                }
                set
                {
                    text = value;
                }
            }

            public void Show()
            {
                Console.WriteLine($"Content: {this.text}");
            }
        }

        static void Main(string[] args)
        {
            Title title = new Title();
            title.Content = "The Catcher in the Rye";

            Author author = new Author();
            author.Content = "J.D. Salinger";

            Content content = new Content();
            content.Text = "This novel is a story about a teenager named Holden Caulfield who is expelled from his prep school and goes on a journey of self-discovery";

            Book book = new Book(title.Content, author.Content, content.Text);
            book.Show(title.Content, author.Content, content.Text);
        } 
    }
}