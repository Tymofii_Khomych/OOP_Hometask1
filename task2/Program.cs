﻿namespace task2
{
    internal class Program
    {
        public class Rectangle
        {
            private double side1 { get; set; }
            private double side2 { get; set; }
            private double area = 0;
            private double perimeter = 0;
            public double Area
            {
                get
                {
                    return side1 * side2;
                }
            }
            public double Perimeter
            {
                get
                {
                    return 2 * (side1 + side2);
                }
            }

            public Rectangle()
            {
                this.side1 = 0;
                this.side2 = 0;
            }
            public Rectangle(double side1, double side2)
            {
                this.side1 = side1;
                this.side2 = side2;
            }


        }
        static void Main(string[] args)
        {
            Console.Write("Enter first side of a rectangle = ");
            double side1 = Convert.ToDouble(Console.ReadLine());
            Console.Write("Enter second side of a rectangle = ");
            double side2 = Convert.ToDouble(Console.ReadLine());

            Rectangle rectangle = new Rectangle(side1, side2);
            Console.WriteLine($"\nArea of rectangle = {rectangle.Area}\nPerimetr of rectangle = {rectangle.Perimeter}");
        }
    }
}