﻿namespace task4
{
    internal class Program
    {
        class Point
        {
            public int x { get; }
            public int y { get; }
            public string name { get; }
            public Point()
            {
                name = string.Empty;    
            }

            public Point(int x, int y)
            {
                this.x = x;
                this.y = y;
                name = $"x = {x}; y = {y}";
            }
        }

        class Figure
        {
            private double first_side;
            private double second_side;
            private double third_side;
            private double fourth_side;
            private double fifth_side;
            private string type { get; }

            Figure()
            {
                type = string.Empty;
            }

            public Figure(Point a, Point b, Point c)
            {
                first_side = LengthSide(a, b);
                second_side = LengthSide(b, c);
                third_side = LengthSide(a, c);
                type = "triangle";
            }
            public Figure(Point a, Point b, Point c, Point d)
            {
                first_side = LengthSide(a, b);
                second_side = LengthSide(b, c);
                third_side = LengthSide(c, d);
                fourth_side = LengthSide(a, d);
                type = "quadrilateral";
            }
            public Figure(Point a, Point b, Point c, Point d, Point e)
            {
                first_side = LengthSide(a, b);
                second_side = LengthSide(b, c);
                third_side = LengthSide(c, d);
                fourth_side = LengthSide(d, e);
                fifth_side = LengthSide(a, e);
                type = "pentagon";
            }

            public void Show()
            {
                Console.WriteLine($"This figure is {type}");
            }

            public double LengthSide(Point a, Point b)
            {
                return Math.Sqrt(Math.Pow(b.x - a.x, 2) + Math.Pow(b.y - a.y, 2));
            }

            public void PerimeterCalculator()
            {
                Console.WriteLine($"Perimeter of {type} = {first_side + second_side + third_side + fourth_side + fifth_side}");
            }
        }

        static void Main(string[] args)
        {
            Point a = new(1, 2);
            Point b = new(3, 4);
            Point c = new(5, 6);
            Point d = new(7, 8);
            Point e = new(9, 10);

            Figure triangle = new(a, b, c);
            triangle.Show();
            triangle.PerimeterCalculator();
            Console.WriteLine();

            Figure quadrilateral = new(a, b, c, d);
            quadrilateral.Show();
            quadrilateral.PerimeterCalculator();
            Console.WriteLine();

            Figure pentagon = new(a, b, c, d, e);
            pentagon.Show();
            pentagon.PerimeterCalculator();
            Console.WriteLine();
        }
    }
}